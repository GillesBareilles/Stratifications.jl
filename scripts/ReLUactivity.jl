using LinearAlgebra
using Distributions
using Random
using Plots

relu(x) = max(0, x)

function barrier!(nLayer_laysize_to_count, laysize, L)
    input = zeros(laysize)
    input[1] = 1.0

    # Generate linear maps
    w = []
    for l in 1:L
        push!(w, rand(Uniform(-1, 1), laysize, laysize))
    end

    # Forward pass
    xₗ = copy(input)
    for l in 1:L
        xₗ .= relu.(w[l] * xₗ)

        if count(v -> v == 0, w[l] * xₗ) > 0
            nLayer_laysize_to_count[l, laysize] += 1
        end
    end
end

function main()
    Random.seed!(143)
    L = 30
    p = 8

    nrep = 10000

    nLayer_laysize_to_count = zeros(L, p)

    for laysize in 1:p
        for indrep in 1:nrep
            barrier!(nLayer_laysize_to_count, laysize, L)
        end
    end

    nLayer_laysize_to_count ./= nrep
    f = heatmap(res, ylabel="nLayer", xlabel="p", title="proba of applying ReLU at a zero, avg of $nrep reps")
    savefig(f, "ReLUactivity.pdf")

    return nLayer_laysize_to_count
end
