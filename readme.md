# Stratifications.jl

Compute stratifications of composition of piecewise linear functions.

See:
- `main.jl` for a computation of a ReLU network stratification computation,
- `scripts/ReLUactivity.jl` for "evaluating relu networks hits nondiff points with high proba".

```julia
include("main.jl")

main()  # relu

main(2) # relu + maxpool
```

## Improvements

- When composing a relu with a maxpool, the naive cartesian product of strata results in the same sets. Indeed, the relu may output a zero vector, which the maxpool will also map to zero. However, applying blindly the 4 strata of maxpool22 results in 4 sets which are the same, and on which the function expression is the same. How to fix this? Take into account the actual image of the map that is consiered, eg the relu of affine here.
- It happens that several strata have the same objective function. These should be merged to get a "minimal" stratification.
