"""
    point_infeasibility(ctrs, x, pt)

Check if point "x=>pt" is feasible for constraints `ctrs`.
"""
function point_infeasibility(ctrs, x, pt)
    infeas = 0.0
    for (ctr, sense) in ctrs
        val = 0.0
        if typeof(ctr) == Float64
            throw(error("xxx"))
            val = ctr
        else
            val = ctr(x => pt)
        end
        if sense == Stratifications.Negative()
            infeas += max(0, val)
        else
            @assert sense == Stratifications.Positive()
            infeas += -min(0, val)
        end
    end
    return infeas
end

function f_stratified(stratification, pt)
    feasstratinds = findall( strata -> (point_infeasibility(strata.constraints, stratification.x, pt) == 0.), stratification.stratas )
    if length(feasstratinds) == 0
        @error "No active stratas at input point" pt
        throw(error())
    elseif length(feasstratinds) == 1
        strata = stratification.stratas[feasstratinds[1]]
        return [ Fi(stratification.x => pt) for Fi in strata.Fexpr ]
    else
        candidates = [ [ Fi(stratification.x => pt) for Fi in strata.Fexpr ] for strata in stratification.stratas[feasstratinds] ]
        dif = [ candidates[i+1] - candidates[i] for i in 1:length(candidates)-1 ]
        if norm(dif) < 1e-14
            return first(candidates)
        else
            @error "Several active strata with different values" pt candidates
            throw(error())
        end
    end
end
