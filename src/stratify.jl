struct Strata
    choices
    constraints
    Fexpr::Vector{Polynomial{DynamicPolynomials.Commutative{DynamicPolynomials.CreationOrder}, Graded{LexOrder}, Float64}}
end

function compute_stratification(net::FeedForwardNet; lmax = -1)
    (lmax == -1) && (lmax = length(net.layers))

    @polyvar x[1:net.inputdim]

    rootstrata = Strata([], [], x .+ 0.)
    prevlayer_strata = [rootstrata]
    curlayer_strata = []

    for l in 1:lmax
        layer = net.layers[l]

        for parentstrata in prevlayer_strata
            for Mrep in stratas(layer)
                # build mapping expression on strata
                Fexprstrata = strata_f(parentstrata.Fexpr, layer, Mrep)
                # build full strata description
                layer_constraints = strata_ctrs(parentstrata.Fexpr, layer, Mrep)
                cur_strataconstraints = vcat(parentstrata.constraints, layer_constraints)

                ## NOTE: check feasibility of constraints, obtain reduced description.
                polyhedron = ctrs_to_hpolyhedron(cur_strataconstraints, x)
                isempty(polyhedron) && continue
                ctrs_cleaned = hpolyhedron_to_ctrs(polyhedron, x)

                # NOTE: sanity check
                if isbounded(polyhedron) && area(polyhedron) == 0.
                    @warn "Non empty strata of zero area!"
                    continue
                end

                strata = Strata(
                    vcat(parentstrata.choices, Mrep),
                    ctrs_cleaned,
                    Fexprstrata,
                )

                push!(curlayer_strata, strata)
            end
        end

        prevlayer_strata = curlayer_strata
        curlayer_strata = []
    end

    return (; x=x, stratas = prevlayer_strata)
end
