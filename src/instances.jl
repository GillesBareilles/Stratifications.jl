function approx_relu()
    lay_one = [
        1.3904 0.5878
        0.5055  0.9271
        -0.4899 -0.0258
        0.1678  0.5434
    ]
    biais_one = [ 0.7072, -0.8182, 0.5257, 0.2919]
    lay_two = [
        0.2788 -0.4558  0.0382 -0.2822
        0.6601 -0.9854 -0.3215  0.2569
    ]
    biais_two = [ -0.3386, 0.0613 ]
    lay_three = [-0.2982 0.8478]
    biais_three = [-0.4105]

    in_offset = [-1.91912641, -1.6514828]
    in_scale = Diagonal(inv.([3.77377745, 3.35386935]))
    net = FeedForwardNet([
        AffineLayer(in_scale, -in_scale*in_offset),
        AffineLayer(lay_one, biais_one),
        PointwiseRelu(4),
        AffineLayer(lay_two, biais_two),
        PointwiseRelu(2),
        AffineLayer(lay_three, biais_three),
        AffineLayer(Diagonal([6.98903187]), [- 3.04015044]),
    ], 2)
    return net
end

function approx_relumaxpool()
    lay1 = [ 2.2361 -0.3008
          -0.1853  0.5550
           0.1644 -1.8787
           0.3808  1.2420]
    bias1 = [ -0.2170, -0.5548,  0.9242, -0.8575]
    lay2 =  [
         1.0924 -1.0364
         1.9273 -1.3428
    ]
    bias2 = [ 0.0611, -0.0713]
    lay3 =  [
        -0.4849 -0.0957
        -0.6094 -0.6376
    ]
    bias3 = [-0.2914,  0.4920]
    lay4 = [-0.6977 -1.0944]
    bias4 = [-0.0979]

    # w0 = (x - [-1.91912641, -1.6514828 ]) ./ [3.83359978, 3.43015847]
    # w1 = relu.(lay1 * w0 + bias1)            # relu
    # w15 = [ maximum(w1[1:2]), maximum(w1[3:4]) ] # maxpool
    # pre2 = lay2 * w15 + bias2
    # w2 = σ.(pre2) # sigmoid
    # pre3 = lay3 * w2 + bias3
    # w3 = tanh.(pre3)
    # out = lay4 * w3 + bias4
    # res = out .* 6.683799249419673 .- 2.8189980623517337

    in_scale = Diagonal(inv.([3.83359978, 3.43015847]))
    in_offset = [-1.91912641, -1.6514828 ]
    net = FeedForwardNet([
        AffineLayer(in_scale, -in_scale*in_offset),
        AffineLayer(lay1, bias1),
        PointwiseRelu(4),
        MaxPool22(),
        # AffineLayer(lay2, bias2),
        # PointwiseSigmoid(),
        # AffineLayer(lay3, bias3),
        # PointwiseTanh(),
        # AffineLayer(lay4, bias4),
        # AffineLayer(Diagonal([6.683799249419673]), [-2.8189980623517337]),
    ], 2)
    return net
end


function get_randweights(p, L; Tf=Float64)
    throw(error("to be implemented"))
    return DenseNet(
        [rand(Uniform(-1, 1), p, p) for l in 1:L],
        [rand(Uniform(-1, 1), p) for l in 1:L],
        [x[i] for i in 1:p],
        L, p)
end

