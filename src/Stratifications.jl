module Stratifications

using DynamicPolynomials
using LazySets
using LinearAlgebra

#################
#### ReLU network
#################
relu(x) = max(0, x)

struct DenseNet{Tf}
    A::Vector{Matrix{Tf}}
    b::Vector{Vector{Tf}}
    nlayers::Int64
    inputdim::Int64
end
include("instances.jl")

function f(net::DenseNet, x; apply_lastlayerrelu=false)
    outlay = copy(x)
    for l in 1:net.nlayers-1
        inlay = net.A[l] * outlay + net.b[l]
        outlay = relu.(inlay)
    end
    inlay = net.A[net.nlayers] * outlay + net.b[net.nlayers]
    outlay = apply_lastlayerrelu ? relu.(inlay) : inlay
    return outlay
end


abstract type AbstractLayer end


struct FeedForwardNet
    layers::Vector
    inputdim::Int64
end

function f(net::FeedForwardNet, x)
    curval = copy(x)
    for layer in net.layers
        curval = f(curval, layer)
    end
    return curval
end

struct AffineLayer{Tf, Tm}
    A::Tm
    b::Vector{Tf}
end
function f(curval, layer::AffineLayer{Tf}) where Tf
    return layer.A * curval .+ layer.b
end
stratas(::AffineLayer) = [:wholespace]
strata_f(curval, layer::AffineLayer, Mrep) = f(curval, layer)
strata_ctrs(curval, ::AffineLayer, Mrep) = []

struct PointwiseRelu
    d::Int64
end
f(curval, ::PointwiseRelu) = relu.(curval)
stratas(relus::PointwiseRelu) = [
    bitstring(val)[end-relus.d+1:end] for val in 0:2^relus.d-1
]
strata_f(curval, ::PointwiseRelu, Mrep) = [ strata_f(curval[i], Mrep[i]) for i in axes(curval, 1)]
strata_f(x, Mrep) = Mrep == '0' ? 0. : x
strata_ctrs(curval, ::PointwiseRelu, Mrep) = [
    strata_ctrs(curval[i], Mrep[i]) for i in axes(curval, 1)
]
strata_ctrs(x, Mrep) = (x, Mrep == '0' ? Negative() : Positive())

struct MaxPool22 end
f(curval, ::MaxPool22) = [ maximum(curval[1:2]), maximum(curval[3:4]) ]
stratas(::MaxPool22) = [(:firstcoord, :firstcoord), (:firstcoord, :secondcoord), (:secondcoord, :firstcoord), (:secondcoord, :secondcoord)]
strata_f(curval, layer::MaxPool22, Mrep) = [ strata_f(curval[1:2], layer, Mrep[1]), strata_f(curval[3:4], layer, Mrep[2]) ]
function strata_f(x, ::MaxPool22, Mrep::Symbol)
    if Mrep == :firstcoord
        return x[1]
    else
        return x[2]
    end
end
strata_ctrs(curval, layer::MaxPool22, Mrep) = vcat(strata_ctrs(curval[1:2], layer, Mrep[1]), strata_ctrs(curval[3:4], layer, Mrep[2]))
function strata_ctrs(x, ::MaxPool22, Mrep::Symbol)
    if Mrep == :firstcoord
        return [(x[1]-x[2], Positive())]
    else
        return [(x[2]-x[1], Positive())]
    end
end



struct PointwiseSigmoid end
σ(t) = 1 / (1+exp(-t))
f(curval, ::PointwiseSigmoid) = σ.(curval)
stratas(::PointwiseSigmoid) = [:wholespace]
strata_f(curval, ::PointwiseSigmoid, Mrep) = σ.(curval)
strata_ctrs(curval, ::PointwiseSigmoid, Mrep) = []

struct PointwiseTanh end
f(curval, ::PointwiseTanh) = tanh.(curval)
stratas(::PointwiseTanh) = [:wholespace]
strata_f(curval, ::PointwiseTanh, Mrep) = tanh.(curval)
strata_ctrs(curval, ::PointwiseTanh, Mrep) = []

abstract type IneqSense end
struct Negative end
struct Positive end

export stratas, strata_f, strata_ctrs


"""
    mismatchstratifcomposite_2d(net, stratif)

Compute the error between the deepnet value in composition form and in stratified form.
"""
function mismatchstratifcomposite_2d(net, stratif; npoints = 1e3)
    @assert length(net.inputdim) == 2

    intval = -50.:1:50.
    diff = 0.
    for ix in intval, iy in intval
        pt = [ix, iy]
        w = f(net, pt)
        x = f_stratified(stratif, pt)
        distvx = norm(w-x)
        distvx > 1e-7 && @show f(net, pt), f_stratified(stratif, pt), pt
        diff = max(diff, distvx)
    end
    return diff
end

"""
    mismatchstratifcomposite(net, stratif)

Compute the error between the deepnet value in composition form and in stratified form.
"""
function mismatchstratifcomposite(net, stratif; npoints = 1e4)
    d = net.inputdim

    diff = 0.
    for i in 1:npoints
        pt = ([ rand() for i in 1:d ] .- 0.5) .* 10
        w = f(net, pt)
        x = f_stratified(stratif, pt)
        distvx = norm(w-x)
        distvx > 1e-7 && @show f(net, pt), f_stratified(stratif, pt), pt
        diff = max(diff, distvx)
    end
    return diff
end

include("ctr_to_lazysets.jl")
include("f_stratified.jl")

include("stratify.jl")
export compute_stratification
export mismatchstratifcomposite

export ctrs_to_hpolyhedron

export FeedForwardNet, AffineLayer, PointwiseRelu, MaxPool22, PointwiseSigmoid, PointwiseTanh
export f
export approx_relu, approx_relumaxpool
end # module Stratifications
