function ctrs_to_hpolyhedron(ctrs, x)
    d = length(x)
    halfspaces = HalfSpace{Float64, Vector{Float64}}[]
    for (ctr, sense) in ctrs
        # write constraint of the form "(a, x) <= b"
        a = [coefficient(ctr, x[i]) for i in 1:d]
        b = -coefficient(ctr, x[1]^0)
        if sense == Positive()
            a *= -1; b *= -1
        end
        if norm(a) == 0. # NOTE: the constraint is a constant polynomial...
            @assert length(variables(ctr)) == 0
            ctrval = ctr(x => zeros(d))
            if (ctrval <= 0 && sense == Negative()) || (ctrval >=0 && sense == Positive())
                continue # feasible, move on to next ctr
            else
                @debug "set here" ctrs
                return EmptySet(length(x))
            end
        end
        halfspace = HalfSpace(a, b)
        push!(halfspaces, halfspace)
    end
    polyhedron = HPolyhedron(halfspaces)
    remove_redundant_constraints!(polyhedron)
    return polyhedron
end

function hpolyhedron_to_ctrs(polyhedron, x)
    ctrs_cleaned = []
    for halfspace in polyhedron.constraints
        p = dot(halfspace.a, x) - halfspace.b
        push!(ctrs_cleaned, (p, Negative()))
    end
    return ctrs_cleaned
end
