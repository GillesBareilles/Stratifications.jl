using Stratifications
using LinearAlgebra
using DynamicPolynomials
using LazySets
using Plots

function main(example=1)
    local net
    if example == 1
        println("Looking at the fisrt example: dense relu network")
        net = approx_relu()
    else
        println("Looking at the second example: relu + maxpool network")
        net = approx_relumaxpool()
    end

    @time stratif =  compute_stratification(net)
    println("stratification built")

    # NOTE: number of different objectives
    Fexprs = Set()
    for stratas in stratif.stratas
        push!(Fexprs, stratas.Fexpr)
    end
    println(" > # full dim strata          : $(length(stratif.stratas))")
    println(" > # different objectives     : $(length(Fexprs))")

    # NOTE: difference between composite and stratified expression
    diffstrcomp = mismatchstratifcomposite(net, stratif)
    println(" > # mismatch (comp vs strat) : ", diffstrcomp)

    # NOTE: plot regions per objectives, and print vertices.
    plt = plot()
    dom = Interval(-2, 2) × Interval(-2, 2)
    count = 0
    plottedsets = Set()
    for (i, strata) in enumerate(stratif.stratas)
        # printstyled(" -> strata : $strata\n", color=:yellow)
        polyh = ctrs_to_hpolyhedron(strata.constraints, stratif.x)

        set = convert(HPolyhedron, polyh ∩ dom)
        verticeslist = vertices_list(set)
        if verticeslist in plottedsets
            # println(" <| already plotted set")
        elseif !isempty(set) && area(set) > 0
            println(" |> area: ", area(set))
            printstyled("    Objective: ", strata.Fexpr, "\n", color=:yellow)
            println(verticeslist)
            plot!(set, lab="P$i")
            plot!(set)
            push!(plottedsets, verticeslist)
            count += 1
        else
            # println(" <| empty set")
        end
    end
    xlims!(plt, (-2,2))
    ylims!(plt, (-2,2))
    println(" > # full-dim strata in [-2, 2]^2: ", count)

    return plt
end
